# rust-blog

#### 介绍
Rust博客，使用Rocket+Rbatis+mysql  
前后端分离，此为rust后端api  
前端前台项目请移步：[rust-blog-home](https://gitee.com/yuguofu/rust-blog-home)  
前端后台项目请移步：[rust-blog-admin](https://gitee.com/yuguofu/rust-blog-admin)  


#### 软件架构
Rocket v0.5-rc + Rbatis 3.0 + mysql  


#### 安装教程

1.  克隆到本地
2.  导入sql
3.  修改数据库连接字符串  
4.  运行cargo run

#### 使用说明

1.  克隆到本地
2.  导入sql
3.  修改数据库连接字符串  
4.  运行cargo run


#### 预览
![输入图片说明](%E6%88%AA%E5%9B%BE/%E9%A6%96%E9%A1%B5.png)
![输入图片说明](%E6%88%AA%E5%9B%BE/%E8%AF%A6%E6%83%85%E9%A1%B5.png)
