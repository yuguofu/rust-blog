CREATE TABLE `article_to_tag` (
  `article_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  KEY `article_to_tag_FK1` (`article_id`),
  KEY `article_to_tag_FK2` (`tag_id`),
  CONSTRAINT `article_to_tag_FK1` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`),
  CONSTRAINT `article_to_tag_FK2` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4


INSERT INTO rustblog.article_to_tag (article_id,tag_id) VALUES
	 (2,3),
	 (2,4),
	 (4,1),
	 (1,4);
