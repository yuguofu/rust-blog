CREATE TABLE `article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `content` text NOT NULL,
  `cate_id` int(10) unsigned NOT NULL,
  `istop` tinyint(1) DEFAULT '0',
  `created_at` bigint(20) DEFAULT NULL,
  `updated_at` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `article_FK` (`cate_id`),
  CONSTRAINT `article_FK` FOREIGN KEY (`cate_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4



INSERT INTO rustblog.article (title,description,content,cate_id,istop,created_at,updated_at) VALUES
	 ('Rust类型转换','Rust类型转换','Rust类型转换Rust类型转换',1,1,1640274685,1640274685),
	 ('winform界面设计','winform界面设计','winform界面设计',2,0,1640274685,1640274685),
	 ('go语言字符串拼接','go语言字符串拼接','go语言字符串拼接',3,0,1640274685,1640274685),
	 ('Rust中方法和关联函数的区别123','Rust中方法和关联函数的区别123','#### 1.定义方式不同
**方法**定义：
```rust
	// self或&self
	fn 方法名(&self, 参数)->返回值{
		// 方法体
	}
```

**关联函数**定义：
```rust
	fn 函数名(参数) 返回值{
		// 函数体
	}

// 例
fn new(x:i32, y:i32) -> Rectangle{
		Rectangle {
			x: x,
			y: y,
		}
	}
```


#### 2.调用方式不同
**方法**调用，使用“.” ，例：
```rust
	rect1.area()
```

**关联函数**调用，使用“::”，类似其他语言的静态方法，例：
```rust
	let a = Rectangle::new(12, 24);
```


###### 示例代码
```rust
	struct Rectangle{
    width:u32,
    height:u32,
}

impl Rectangle{
	// new 关联函数
	fn new(x:i32, y:i32) -> Rectangle{
		Rectangle {
			x: x,
			y: y,
		}
	}

	//这个结构体拥有求面积的方法
	fn area(&self)->u32{
		 return self.width * self.height;
	}

}

fn main() {
	let rect1 = Rectangle::new(12, 24);
	println!("the rectangle''s area is {}",rect1.area());
}
```',1,0,1640274685,1641480143),
	 ('Rust中方法和关联函数的区别','Rust中方法和关联函数的区别','#### 1.定义方式不同
**方法**定义：
```rust
	// self或&self
	fn 方法名(&self, 参数)->返回值{
		// 方法体
	}
```

**关联函数**定义：
```rust
	fn 函数名(参数) 返回值{
		// 函数体
	}

// 例
fn new(x:i32, y:i32) -> Rectangle{
		Rectangle {
			x: x,
			y: y,
		}
	}
```


#### 2.调用方式不同
**方法**调用，使用“.” ，例：
```rust
	rect1.area()
```

**关联函数**调用，使用“::”，类似其他语言的静态方法，例：
```rust
	let a = Rectangle::new(12, 24);
```


###### 示例代码
```rust
	struct Rectangle{
    width:u32,
    height:u32,
}

impl Rectangle{
	// new 关联函数
	fn new(x:i32, y:i32) -> Rectangle{
		Rectangle {
			x: x,
			y: y,
		}
	}

	//这个结构体拥有求面积的方法
	fn area(&self)->u32{
		 return self.width * self.height;
	}

}

fn main() {
	let rect1 = Rectangle::new(12, 24);
	println!("the rectangle''s area is {}",rect1.area());
}
```',1,0,1640274685,1640274685),
	 ('openSUSE Tumbleweed安装NVIDIA显卡驱动','openSUSE Tumbleweed安装NVIDIA显卡驱动','## 安装过程
1. 首先删除 `/etc/X11/xorg.conf` 文件。然后查看所有位于 `/etc/X11/xorg.conf.d` 下的文件，确保其中不包含任何关于 ServerLayout、Device 和 Screen 的配置文件。
2. 安装 suse-prime:
```bash
zypper install suse-prime
```
## 使用
要在intel显卡和NVIDIA显卡之间转换，可以运行：
```bash
sudo prime-select nvidia
```
或者
```bash
sudo prime-select intel
```
然后**注销**并重新登录以应用显卡变更',8,0,1640274685,1640274685);
