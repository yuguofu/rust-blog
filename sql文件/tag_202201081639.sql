CREATE TABLE `tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `created_at` bigint(20) DEFAULT NULL,
  `updated_at` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4



INSERT INTO rustblog.tag (name,created_at,updated_at) VALUES
	 ('net core',1640274685,1640274685),
	 ('opensuse',1640274685,1641357336),
	 ('winform',1640274685,1640274685),
	 ('rocket',1640274685,1640274685),
	 ('thinkPHP',1640528569,1640528593);
