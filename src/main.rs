mod controller;
mod model;
mod response;
mod util;

#[macro_use]
extern crate rbatis;

#[macro_use]
extern crate lazy_static;

use rbatis::rbatis::Rbatis;
use rocket::fairing::AdHoc;
// use rocket::fs::{relative, FileServer};
use rocket::serde::json::{serde_json::json, Value};
use rocket::{catch, catchers, routes, Request};
use std::sync::Arc;

use jwt_simple::prelude::HS256Key;

use crate::controller::article_controller;
use crate::controller::category_controller;
use crate::controller::tag_controller;
use crate::controller::user_controller;
use response::resp_obj::RespData;

// 定义全局变量
lazy_static! {
    // Rbatis类型变量 RB，用于数据库查询
    static ref RB: Rbatis = Rbatis::new();
    // HS256Key类型变量 KEY，用于jwt
    static ref KEY: HS256Key = HS256Key::generate();
}

// 处理404错误的函数
#[catch(404)]
fn not_found(_req: &Request) -> Value {
    json!(RespData {
        code: 404,
        msg: "未找到相关内容...",
        data: ()
    })
}

// mod home {
//     use rocket::{fs::NamedFile, response::Redirect, uri};
//     use std::path::{Path, PathBuf};

//     /// 静态文件代理
//     #[rocket::get("/<path..>")]
//     pub async fn home(path: PathBuf) -> Option<NamedFile> {
//         let mut path = Path::new(super::relative!("static")).join(path);
//         if path.is_dir() {
//             path.push("/index.html");
//         }

//         NamedFile::open(path).await.ok()
//     }

//     /// '/'重定向到'/index.html'
//     #[rocket::get("/")]
//     pub fn index() -> Redirect {
//         Redirect::to(uri!("/index.html"))
//     }
// }



#[rocket::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    //启用日志输出，你也可以使用其他日志框架，这个不限定的
    fast_log::init_log("requests.log", 1000, log::Level::Info, None, true);

    //初始化数据库连接池
    RB.link("mysql://root:123456@127.0.0.1:3306/rustblog")
        .await
        .unwrap();

    let rb = Arc::new(&RB);

    // 自定端口
    // let figment = rocket::Config::figment()
    //     .merge(("port", 7777));
    // rocket::custom(figment)

    rocket::build()
        .register("/", catchers![not_found])
        .mount(
            "/api",
            routes![
                user_controller::login,                     //登录
                user_controller::list,                      //用户列表
                user_controller::delete,                    //删除用户
                category_controller::list,                  //分类列表
                category_controller::detail,                //分类详情
                category_controller::cate_artlist,          //分类下文章
                category_controller::create,                //新增分类
                category_controller::update,                //更新分类
                category_controller::delete,                //删除分类
                article_controller::list,                   //文章列表
                article_controller::detail,                 //文章详情
                article_controller::editing_article_detail, //正在编辑的文章详情
                article_controller::hot,                    //最热文章
                article_controller::create,                 //新增文章
                article_controller::delete,                 //删除文章
                article_controller::update,                 //更新文章
                article_controller::search,                 //后台文章管理搜索
                article_controller::home_search,            //前台搜索输入框搜索
                tag_controller::list,                       //标签列表
                tag_controller::tag_artlist,                //某标签下文章
                tag_controller::create,                     //新增标签
                tag_controller::update,                     //更新标签
                tag_controller::remove,                     //删除标签
            ],
        )

	// .mount("/", rocket::routes![home::home, home::index])
        // .mount("/", FileServer::from(relative!("static")))

        .attach(AdHoc::on_ignite("Rbatis Database", |rocket| async move {
            rocket.manage(rb)
        }))
        .launch()
        .await?;
    Ok(())
}
