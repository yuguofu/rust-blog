// use super::status_code::StatusCode;
use serde::Serialize;

/// 封装响应数据的结构体1
#[derive(Serialize)]
pub struct RespData<'a, T> {
    pub code: u32,
    pub msg: &'a str,
    pub data: T,
}

/// 封装响应数据的结构体2（带分页信息）
#[derive(Serialize)]
pub struct RespData2<'a, T> {
    pub code: u32,
    pub msg: &'a str,
    pub data: T,
    pub current_page: u64,
    pub page_size: u64,
    pub total: u64,
}
